﻿using System;
using System.Collections.Generic;
using FluentAssertions;
using FluentValidation.Results;
using Reviews.Application.Common.Exceptions;
using Xunit;

namespace Reviews.Application.UnitTests.Common.Exceptions;

public class ValidationExceptionTests
{
    [Fact]
    public void NoFailuresNoErrors()
    {
        var subject = new ValidationException().Errors;

        subject.Keys.Should().BeEquivalentTo(Array.Empty<string>());
    }

    [Fact]
    public void SingleFailureCreatesSingleError()
    {
        //Arrange
        var failures = new List<ValidationFailure>
        {
            new ("Name", "Name must not be empty"),
        };

        //Act
        var subject = new ValidationException(failures).Errors;

        //Assert
        subject.Keys.Should().BeEquivalentTo("Name");
        subject["Name"].Should().BeEquivalentTo("Name must not be empty");
    }

    [Fact]
    public void MultiplyFailuresForSamePropertyShouldBeGrouped()
    {
        //Arrange
        var failures = new List<ValidationFailure>
        {
            new("Name", "Name must not be empty"),
            new("Name", "Name must be less than 120 characters"),
            new("Text", "Text must not be empty"),
            new("Text", "Text must be less than 2000 characters")
        };

        //Act
        var subject = new ValidationException(failures).Errors;

        //Assert
        subject.Keys.Should().BeEquivalentTo("Name", "Text");
        subject["Name"].Should().BeEquivalentTo("Name must not be empty", "Name must be less than 120 characters");
        subject["Text"].Should().BeEquivalentTo("Text must not be empty", "Text must be less than 2000 characters");
    }
}