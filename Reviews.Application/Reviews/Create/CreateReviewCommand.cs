using MediatR;
using Reviews.Application.Common.Interfaces;
using Reviews.Domain.Entities;
using Reviews.Domain.Interfaces;

namespace Reviews.Application.Reviews.Create;

public record CreateReviewCommand(
    string? Name,
    string? Text,
    DateTime StartDate,
    DateTime EndDate
    ) : IRequest<CreateReviewCommandResult>;

public record CreateReviewCommandResult(long Id, DateTime CreatedDate);

public class Handler : IRequestHandler<CreateReviewCommand, CreateReviewCommandResult>
{
    private readonly IApplicationContext _context;
    private readonly IDateTime _dateTime;

    public Handler(IApplicationContext context, IDateTime dateTime) => 
        (_context,_dateTime) = (context, dateTime);

    public async Task<CreateReviewCommandResult> Handle(CreateReviewCommand request, CancellationToken cancellationToken)
    {
        var review = new Review(request.Name, request.Text, request.StartDate, request.EndDate, _dateTime);

        _context.Reviews.Add(review);

        await _context.SaveChangesAsync(cancellationToken);
        
        return new CreateReviewCommandResult(review.Id, review.CreatedDate);
    }
}