using MediatR;
using Microsoft.AspNetCore.Mvc;
using Reviews.Application.Reviews.Create;
using Reviews.Application.Reviews.List;

namespace Reviews.Api.Controllers;

[ApiController]
[Route("api/[controller]")]
public class ReviewsController : ControllerBase
{
    private readonly IMediator _mediator;

    public ReviewsController(IMediator mediator) => _mediator = mediator;

    [HttpGet]
    public async Task<ListReviewViewModel[]> List() => 
        await _mediator.Send(new ListReviewsQuery());

    [HttpPost]
    public async Task<CreateReviewCommandResult> Create([FromBody] CreateReviewCommand command) =>
        await _mediator.Send(command);
}