import { Card, CardContent, CardHeader, Typography } from "@mui/material";
import moment from "moment";
import React from "react";
import Review from "../models/review";

const ReviewItem: React.FC<{ review: Review }> = (props) => {
    const review = props.review;

    function formatDate(d: string): string {
        return moment(d).format('D/MM/YY');
    }

    return (
        <Card sx={{ maxWidth: 345 }}>
            <CardHeader
                title={review.name}
                subheader={formatDate(review.startDate) + ' ‒ ' + formatDate(review.endDate)}

            />
            <CardContent>
                <Typography variant="caption" color="text.grey">
                    ({formatDate(review.createdDate)})
                </Typography>
                <Typography variant="body2" color="text.secondary">
                    {review.text}
                </Typography>

            </CardContent>
        </Card>
    );
}

export default ReviewItem;