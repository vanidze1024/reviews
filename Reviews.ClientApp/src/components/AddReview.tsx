import { Button, CircularProgress, Dialog, DialogActions, DialogContent, DialogTitle, Stack, TextField } from "@mui/material";
import React, { useState } from "react";
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import Review from "../models/review";
import Api from '../api/reviews.api';
import CreateReviewCommand from "../models/createReviewCommand";

const AddReview: React.FC<{ onAddReview: (r: Review) => void }> = (props) => {
    const [show, setShow] = useState(false);
    const [isLoading, setLoading] = useState(false);

    const [name, setName] = useState<string>();
    const [text, setText] = useState<string>();
    const [startDate, setStartDate] = useState<string>();
    const [endDate, setEndDate] = useState<string>();

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const submitHandler = async (event: React.MouseEvent) => {
        event.preventDefault();

        if (!name || name.trim().length === 0 || (name && name.length > 120)) {
            return;
        }
        if (!text || text.trim().length === 0) {
            return;
        }
        if (!startDate || startDate.trim().length === 0) {
            return;
        }
        if (!endDate || endDate.trim().length === 0) {
            return;
        }

        try {
            setLoading(true);

            const { id, createdDate } = await Api.createReview({
                name,
                text,
                startDate,
                endDate
            } as CreateReviewCommand);

            props.onAddReview({
                id,
                name,
                text,
                createdDate,
                startDate,
                endDate
            } as Review);

            setShow(false);
        } finally {
            setLoading(false);
        }

    }

    return (
        <>
            <div>
                <Button variant="outlined"
                    startIcon={<AddCircleOutlineIcon />}
                    onClick={handleShow}>Add</Button>
            </div>

            <Dialog open={show} onClose={handleClose}>
                <DialogTitle>Create new Review</DialogTitle>
                <DialogContent>
                    <Stack spacing={2} width={400}>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="name"
                            label="Name (Max. 120)"
                            type="text"
                            fullWidth
                            variant="standard"
                            value={name}
                            onChange={(e) => setName(e.target.value)}
                        />
                        <TextField
                            id="startDate"
                            label="Start Date"
                            type="date"
                            fullWidth
                            InputLabelProps={{
                                shrink: true,
                            }}
                            value={startDate}
                            onChange={(e) => setStartDate(e.target.value)}
                        />
                        <TextField
                            id="endDate"
                            label="End Date"
                            type="date"
                            fullWidth
                            InputLabelProps={{
                                shrink: true,
                            }}
                            value={endDate}
                            onChange={(e) => setEndDate(e.target.value)}
                        />
                        <TextField
                            id="text"
                            label="Text"
                            multiline
                            rows={5}
                            fullWidth
                            value={text}
                            onChange={(e) => setText(e.target.value)}
                        />
                    </Stack>

                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} disabled={isLoading}>Cancel</Button>
                    <Button onClick={submitHandler}
                        variant="contained"
                        disabled={isLoading}>
                        {isLoading && <CircularProgress />}
                        {!isLoading && 'Create'}
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    );
}

export default AddReview;