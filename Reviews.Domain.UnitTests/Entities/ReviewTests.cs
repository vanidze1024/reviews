using System;
using FluentAssertions;
using NSubstitute;
using Reviews.Domain.Entities;
using Reviews.Domain.Interfaces;
using Xunit;

namespace Reviews.Domain.UnitTests.Entities;

public class ReviewTests
{
    [Fact]
    public void AllPropsShouldBeInitialized()
    {
        //Arrange
        var name = "name";
        var text = "text";
        var startDate = DateTime.Today.AddDays(-2);
        var endDate = DateTime.Today.AddDays(-1);
        var createdDate = DateTime.Today;
        
        var dateTime = Substitute.For<IDateTime>();
        dateTime.Now.Returns(createdDate);
        
        //Act
        var subject = new Review(name, text, startDate, endDate, dateTime);

        //Asset
        subject.Name.Should().Be(name);
        subject.Text.Should().Be(text);
        subject.StartDate.Should().Be(startDate);
        subject.EndDate.Should().Be(endDate);
        subject.CreatedDate.Should().Be(createdDate);
    }
}