﻿using Reviews.Domain.Interfaces;

namespace Reviews.Infrastructure;

public class DateTimeService : IDateTime
{
    public DateTime Now => DateTime.Now;
}
