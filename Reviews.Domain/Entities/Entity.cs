namespace Reviews.Domain.Entities;

public abstract class Entity
{
    public long Id { get; private set; }
}