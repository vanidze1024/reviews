using Microsoft.EntityFrameworkCore;
using Reviews.Domain.Entities;

namespace Reviews.Application.Common.Interfaces;

public interface IApplicationContext
{
    public DbSet<Review> Reviews { get;}
    public Task<int> SaveChangesAsync(CancellationToken cancellationToken);
}