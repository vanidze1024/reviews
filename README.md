Technical assigment for Relex company.
Solutions contains two applications for managing reviews:

## .NET API
https://reviews-api-ivan.azurewebsites.net/swagger/index.html

API has two endpoints
- /api/reviews [GET] - to retrieve list of all Reviews
- /api/reviews [POST] - to add new Review


Application deployed to **Docker** container, then uploading to **Docker Hub**, after that it deploys to **Azure App Service**. All records are stored in **SQL Server** database hosted in **Azure**.

## Client App(React)
https://vigorous-thompson-cdf40b.netlify.app/

Client application is written on React, Typesript and [MUI UI framework](https://mui.com/) .


## TODO:
- Write more unit tests
- Write integration tests
- Write test for React components
- Add better validation on client
- Add Dev/Staging/Production branches
- Add health checks
- Deploy to Kubernetes
- Optimize CI

