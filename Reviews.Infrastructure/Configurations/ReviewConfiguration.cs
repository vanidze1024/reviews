using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Reviews.Domain.Entities;

namespace Reviews.Infrastructure.Configurations;

internal class ReviewConfiguration : EntityConfiguration<Review>, IEntityTypeConfiguration<Review>
{
    public void Configure(EntityTypeBuilder<Review> builder)
    {
        ConfigureBase(builder);

        builder.Property(x => x.Name).HasMaxLength(120).IsRequired();
        //TODO: find out rules for Text
        builder.Property(x => x.Text).HasColumnType("ntext").IsRequired();
        builder.Property(x => x.CreatedDate).IsRequired();
        builder.Property(x => x.StartDate).IsRequired();
        builder.Property(x => x.EndDate).IsRequired();

        builder.ToTable("Reviews", "test");
    }
}