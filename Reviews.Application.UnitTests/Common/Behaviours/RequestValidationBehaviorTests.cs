﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using FluentValidation;
using FluentValidation.Results;
using MediatR;
using NSubstitute;
using Reviews.Application.Common.Behaviours;
using Xunit;
using ValidationException = Reviews.Application.Common.Exceptions.ValidationException;

namespace Reviews.Application.UnitTests.Common.Behaviours;

public class RequestValidationBehaviorTests
{
    [Fact]
    public async Task ShouldThrowValidationExceptionIfOneOfValidatorsFails()
    {
        //Arrange
        var command = Substitute.For<IRequest<int>>();
        var validator = Substitute.For<IValidator<IRequest<int>>>();
        var next = Substitute.For<RequestHandlerDelegate<int>>();
        var failures = new []
        {
            new ValidationFailure("property", "error")
        };
        var expected = new ValidationException(failures);
        validator.Validate(command).Returns(new ValidationResult(failures));
        
        //Act
        var sut = new RequestValidationBehavior<IRequest<int>, int>(new []{validator});
        var sutAction = async () => await sut.Handle(command, CancellationToken.None, next);

        //Assert
        (await sutAction
            .Should().ThrowAsync<ValidationException>())
            .Which.Errors.Should().BeEquivalentTo(expected.Errors);
    }
    
    [Fact]
    public async Task ShouldNotThrowValidationPasses()
    {
        //Arrange
        var command = Substitute.For<IRequest<int>>();
        var validator = Substitute.For<IValidator<IRequest<int>>>();
        var next = Substitute.For<RequestHandlerDelegate<int>>();
        validator.Validate(command).Returns(new ValidationResult(Enumerable.Empty<ValidationFailure>()));
        
        //Act
        var sut = new RequestValidationBehavior<IRequest<int>, int>(new []{validator});
        var sutAction = async () => await sut.Handle(command, CancellationToken.None, next);

        //Assert
        await sutAction.Should().NotThrowAsync();
    }
    
    [Fact]
    public async Task ShouldNotThrowValidationIsNull()
    {
        //Arrange
        var command = Substitute.For<IRequest<int>>();
        var validator = Substitute.For<IValidator<IRequest<int>>>();
        var next = Substitute.For<RequestHandlerDelegate<int>>();
        validator.Validate(command).Returns(new ValidationResult(new ValidationFailure?[] {null}));
        
        //Act
        var sut = new RequestValidationBehavior<IRequest<int>, int>(new []{validator});
        var sutAction = async () => await sut.Handle(command, CancellationToken.None, next);

        //Assert
        await sutAction.Should().NotThrowAsync();
    }
}
