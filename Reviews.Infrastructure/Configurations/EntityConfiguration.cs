using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Reviews.Domain.Entities;

namespace Reviews.Infrastructure.Configurations
{
    public class EntityConfiguration<T> where T : Entity
    {
        protected void ConfigureBase(EntityTypeBuilder<T> builder)
        {
            builder.Property(x => x.Id).UseIdentityColumn();
            builder.HasKey(x => x.Id);
            
            builder.Property<byte[]>("Timestamp").IsRequired().IsRowVersion();
        }
    }
}