using MediatR;
using Microsoft.EntityFrameworkCore;
using Reviews.Application.Common.Interfaces;

namespace Reviews.Application.Reviews.List;

public record ListReviewsQuery : IRequest<ListReviewViewModel[]>;

public record ListReviewViewModel(
    long Id,
    string Name,
    string Text,
    DateTime CreatedDate,
    DateTime StartDate,
    DateTime EndDate
);

public class Handler : IRequestHandler<ListReviewsQuery, ListReviewViewModel[]>
{
    private readonly IApplicationContext _context;

    public Handler(IApplicationContext context) => _context = context;

    public async Task<ListReviewViewModel[]> Handle(ListReviewsQuery request, CancellationToken cancellationToken) =>
        await _context.Reviews
            .AsNoTracking()
            .Select(x => new ListReviewViewModel(
                x.Id,
                x.Name,
                x.Text,
                x.CreatedDate,
                x.StartDate,
                x.EndDate
            ))
            .ToArrayAsync(cancellationToken);
}