﻿FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["Reviews.Api/Reviews.Api.csproj", "Reviews.Api/"]
COPY ["Reviews.Infrastructure/Reviews.Infrastructure.csproj", "Reviews.Infrastructure/"]
COPY ["Reviews.Domain/Reviews.Domain.csproj", "Reviews.Domain/"]
COPY ["Reviews.Application/Reviews.Application.csproj", "Reviews.Application/"]
RUN dotnet restore "Reviews.Api/Reviews.Api.csproj"
COPY . .
WORKDIR "/src/Reviews.Api"
RUN dotnet build "Reviews.Api.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Reviews.Api.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Reviews.Api.dll"]
