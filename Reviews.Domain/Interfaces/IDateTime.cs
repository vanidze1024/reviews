﻿using System;

namespace Reviews.Domain.Interfaces;

public interface IDateTime
{
    DateTime Now { get; }
}
