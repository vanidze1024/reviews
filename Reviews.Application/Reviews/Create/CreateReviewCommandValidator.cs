using FluentValidation;

namespace Reviews.Application.Reviews.Create;

public class CreateReviewCommandValidator : AbstractValidator<CreateReviewCommand>
{
    public CreateReviewCommandValidator()
    {
        RuleFor(x => x.Name).MaximumLength(120).NotEmpty();
        //TODO: find out rules for Text
        RuleFor(x => x.Text).NotEmpty();
        RuleFor(x => x.StartDate).LessThanOrEqualTo(x => x.EndDate);
    }
}