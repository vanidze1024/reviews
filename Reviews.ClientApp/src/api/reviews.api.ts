import axios from 'axios';
import config from '../config';
import Review from '../models/review';
import CreateReviewCommand from '../models/createReviewCommand';


class Api {
    async loadReviews(): Promise<Review[]> {
        const { data } = await axios.get(`${config.apiUrl}/reviews`);
        return data as Review[];
    }

    async createReview(command: CreateReviewCommand): Promise<{id: number, createdDate: string}> {
        const { data } = await axios.post(`${config.apiUrl}/reviews`,  command);
        return data as {id: number, createdDate: string};
    }
}

const api = new Api();

export default api;