using System;
using Reviews.Domain.Interfaces;

namespace Reviews.Domain.Entities;

public class Review : Entity
{
    private Review()
    {
    }
    
    public Review(string name, string text, DateTime startDate, DateTime endDate, IDateTime dateTime)
    {
        CreatedDate = dateTime.Now;
        Name = name;
        Text = text;
        StartDate = startDate;
        EndDate = endDate;
    }

    public string Name { get; private set; } = string.Empty;
    public string Text { get; private set; } = string.Empty;
    public DateTime CreatedDate { get; private set; }
    public DateTime StartDate { get; private set; }
    public DateTime EndDate { get; private set; }
}