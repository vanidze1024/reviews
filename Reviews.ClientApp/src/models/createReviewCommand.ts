//TODO: add generation from swagger

interface CreateReviewCommand {
    name: string;
    text: string;
    startDate: string;
    endDate: string;
}

export default CreateReviewCommand;