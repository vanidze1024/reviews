﻿using FluentValidation.Results;

namespace Reviews.Application.Common.Exceptions;

public class ValidationException : Exception
{
    public ValidationException() => Errors = new Dictionary<string, string[]>();

    public ValidationException(IEnumerable<ValidationFailure> failures) =>
        Errors = failures
            .GroupBy(e => e.PropertyName, e => e.ErrorMessage)
            .ToDictionary(g => g.Key, g => g.ToArray());

    public IDictionary<string, string[]> Errors { get; }
}
