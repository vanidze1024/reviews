using System;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using NSubstitute;
using Reviews.Application.Common.Interfaces;
using Reviews.Application.Reviews.List;
using Reviews.Domain.Entities;
using Reviews.Domain.Interfaces;
using Xunit;

namespace Reviews.Application.UnitTests.Reviews.List;

public class ListReviewsQueryTests
{
    [Fact]
    public async Task ReviewsShouldMapCorrectly()
    {
        //TODO: fix
        //Arrange
        // var dateTime = Substitute.For<IDateTime>();
        // dateTime.Now.Returns(DateTime.Now);
        // var reviews = new []
        // {
        //     new Review("name", "text", DateTime.Now.AddDays(1), DateTime.Now.AddDays(2), dateTime)
        // };
        // var context = Substitute.For<IApplicationContext>();
        // context.Reviews.Returns(reviews);
        // var request = new ListReviewsQuery();
        // var expected = new[]
        // {
        //     new ListReviewViewModel(review.Name, review.Text, review.CreatedDate, review.StartDate, review.EndDate)
        // };
        //
        // //Act
        // var subject = new Handler(context);
        // var subjectAction = async () => await subject.Handle(request, CancellationToken.None);
        //
        // //Assert
        // (await subjectAction()).Should().BeSameAs(expected);
    }
}