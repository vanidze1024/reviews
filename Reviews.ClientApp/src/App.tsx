import '@mui/icons-material';
import './App.css';
import Reviews from './components/Reviews';


function App() {
  return (
    <div>
      <Reviews/>
    </div>
  );
}

export default App;
