using System;
using System.Linq;
using FluentValidation.TestHelper;
using Reviews.Application.Reviews.Create;
using Xunit;

namespace Reviews.Application.UnitTests.Reviews.Create;

public class CreateReviewCommandValidatorTests
{
    private readonly CreateReviewCommandValidator _sut;

    public CreateReviewCommandValidatorTests() => _sut = new CreateReviewCommandValidator();

    [Theory]
    [InlineData("")]
    [InlineData(null)]
    public void ShouldFailWhen_Name_EmptyOrNull(string name)
    {
        var request = new CreateReviewCommand(name, "text", DateTime.Now, DateTime.Now);

        _sut.TestValidate(request).ShouldHaveValidationErrorFor(x => x.Name);
    }
    
    [Fact]
    public void ShouldFailWhen_Name_IsTwoLong()
    {
        var name = string.Join(string.Empty, Enumerable.Range(0, 121).Select(_ => "a"));
        var request = new CreateReviewCommand(name, "text", DateTime.Now, DateTime.Now);

        _sut.TestValidate(request).ShouldHaveValidationErrorFor(x => x.Name);
    }
    
    [Theory]
    [InlineData("")]
    [InlineData(null)]
    public void ShouldFailWhen_Text_IsEmptyOrNull(string text)
    {
        var request = new CreateReviewCommand("name", text, DateTime.Now, DateTime.Now);

        _sut.TestValidate(request).ShouldHaveValidationErrorFor(x => x.Text);
    }
    
    [Fact]
    public void ShouldFailWhen_StartDate_Is_FurtherThan_EndDate()
    {
        var request = new CreateReviewCommand("name", "text", DateTime.Now.AddDays(1), DateTime.Now);

        _sut.TestValidate(request).ShouldHaveValidationErrorFor(x => x.StartDate);
    }
    
    [Fact]
    public void ShouldPassWhenForCorrectData()
    {
        var request = new CreateReviewCommand("name", "text", DateTime.Now, DateTime.Now);

        _sut.TestValidate(request).ShouldNotHaveAnyValidationErrors();
    }
}