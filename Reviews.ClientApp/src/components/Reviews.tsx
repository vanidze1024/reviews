import Review from "../models/review";
import ReviewItem from './ReviewItem';
import AddReview from './AddReview';
import Api from '../api/reviews.api';
import { CircularProgress, Container, Grid, Stack } from "@mui/material";
import { useEffect, useState } from "react";

const Reviews: React.FC = () => {


    const [reviews, setReviews] = useState<Review[]>([]);
    const [isLoading, setLoading] = useState<Boolean>(false);

    useEffect(() => {
        loadReviews();
    }, []);

    async function loadReviews() {
        try {
            setLoading(true);
            const reviews = await Api.loadReviews();
            setReviews(reviews);
        } finally {
            setLoading(false);
        }
    }

    const addReviewHandler = (r: Review) => {
        setReviews((old) => {
            return [...old, r];
        });
    }

    return (
        <Container>
            <Stack direction="row" mt={2} mb={3} justifyContent="space-between">
                <h2>Reviews</h2>
                <AddReview onAddReview={addReviewHandler} />
            </Stack>
            {isLoading && <CircularProgress color="secondary" />}
            {!isLoading && <Grid container
                spacing={{ xs: 2, md: 4 }}
                columns={{ xs: 4, sm: 8, md: 12 }}>

                {reviews.map(x =>
                    <Grid item xs={2} sm={4} md={4} key={x.id}>
                        <ReviewItem key={x.id} review={x} />
                    </Grid>)}

            </Grid>}
        </Container>
    );
}

export default Reviews;