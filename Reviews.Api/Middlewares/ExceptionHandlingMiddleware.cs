using System.Text.Json;
using FluentValidation;

namespace Reviews.Api.Middlewares;

internal sealed class ExceptionHandlingMiddleware : IMiddleware
{
    public async Task InvokeAsync(HttpContext context, RequestDelegate next)
    {
        try
        {
            await next(context);
        }
        catch (ValidationException e)
        {
            await HandleValidationException(context, e);
        }
    }

    private static async Task HandleValidationException(HttpContext httpContext, ValidationException exception)
    {
        httpContext.Response.ContentType = "application/json";
        httpContext.Response.StatusCode = StatusCodes.Status400BadRequest;

        await httpContext.Response.WriteAsync(JsonSerializer.Serialize(exception));
    }
}