using Microsoft.EntityFrameworkCore;
using Reviews.Application.Common.Interfaces;
using Reviews.Domain.Entities;

namespace Reviews.Infrastructure;

public class ReviewsContext : DbContext, IApplicationContext
{
    public ReviewsContext(DbContextOptions<ReviewsContext> options) : base(options)
    {
    }

    public DbSet<Review> Reviews { get; private set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(GetType().Assembly);

        foreach (var entityType in modelBuilder.Model.GetEntityTypes())
        {
            foreach (var relationship in entityType.GetForeignKeys())
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
        }
    }
}