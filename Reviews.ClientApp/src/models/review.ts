//TODO: add generation from swagger

interface Review {
    id: number,
    name: string;
    text: string;
    createdDate: string;
    startDate: string;
    endDate: string;
}

export default Review;