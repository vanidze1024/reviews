using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Net.Http.Headers;
using Reviews.Api.Middlewares;
using Reviews.Application;
using Reviews.Application.Common.Behaviours;
using Reviews.Application.Common.Interfaces;
using Reviews.Domain.Interfaces;
using Reviews.Infrastructure;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddCors();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddDbContext<ReviewsContext>(x =>
{
    x.UseSqlServer(builder.Configuration.GetConnectionString("Default"));
});

builder.Services.AddTransient<IDateTime, DateTimeService>();
builder.Services.AddScoped<IApplicationContext, ReviewsContext>();
builder.Services.AddMediatR(typeof(AssemblyMarker));
builder.Services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestValidationBehavior<,>));
builder.Services.AddValidatorsFromAssembly(typeof(AssemblyMarker).Assembly, includeInternalTypes: true);
builder.Services.AddTransient<ExceptionHandlingMiddleware>();

builder.Services.AddHealthChecks()
    .AddDbContextCheck<ReviewsContext>();
//TODO: add health checks

var app = builder.Build();

// Configure the HTTP request pipeline.
// if (app.Environment.IsDevelopment())
// {
    app.UseSwagger();
    app.UseSwaggerUI();
//}

app.UseHttpsRedirection();

app.UseCors(x =>
{
    //TODO: fix hadrcode
    x.WithOrigins(app.Environment.IsDevelopment()
            ? "http://localhost:3000"
            : "https://*.azurestaticapps.net")
        .SetIsOriginAllowedToAllowWildcardSubdomains();

    x.WithMethods(
        HttpMethods.Put,
        HttpMethods.Post,
        HttpMethods.Delete,
        HttpMethods.Options);

    x.WithHeaders(HeaderNames.ContentType);
    x.AllowCredentials();
});

app.UseMiddleware<ExceptionHandlingMiddleware>();

app.MapControllers();

app.Run();